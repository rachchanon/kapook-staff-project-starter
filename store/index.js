// state เป็นการประกาศตัวแปร เพื่อผูกเข้ากับ data เพื่อส่งไปให้ component
// สามารถมี state ได้เพียงอันเดียวเท่านั้น
export const state = () => ({
  loadedPosts: [],
  isSidebarActive: false,
  themeColor: '#2962ff',

  userList:[//dummy user
    {
      id:1,
      email:'john@mail.com',
      pass:'123',
      username:'john555',
      name:'John',
      lname:'Doe',
      phone:'0894455666',
      position:'Web Developer',
      gender:'male',
      token:'111AAAaaa',
      avatar:'../../assets/images/users/5.jpg',
    },
    {
      id:2,
      email:'macha@mail.com',
      pass:'abc',
      username:'Machy',
      name:'macha',
      lname:'rosali',
      phone:'025321458',
      position:'Content Editor',
      gender:'female',
      token:'222BBBbbb',
      avatar:'../../assets/images/users/8.jpg',
    },
    {
      id:3,
      email:'olivia@mail.com',
      pass:'0000',
      username:'Olive',
      name:'olivia',
      lname:'panatinigos',
      phone:'0835469875',
      position:'Programer',
      gender:'female',
      token:'333CCCccc',
      avatar:'../../assets/images/users/4.jpg',
    },
    {
      id:4,
      email:'birduthen@gmail.com',
      pass:'12345678',
      username:'birdyman',
      name:'sarawut',
      lname:'yong',
      phone:'0812564587',
      position:'Front-End Developer',
      gender:'male',
      token:'444DDDddd',
      avatar:'../../assets/images/users/2.jpg',
    },
  ],
  tmpCrop:'',
  total_list:4,
  mail_create:'',
  forgot:'',
  token:'',
  facebookData:'',
  googleData:{
    id: '',
    name: '',
    fname: '',
    lname: '',
    image: '',
    email: '',
  },

});

// mutations เป็นการกำหนดให้ตัวแปรต้นทางเกิดการ mutate
// mutations เป็นการย้าย logic จากที่เคยเรียกตรงๆ จากที่อื่นเข้ามาไว้ใน store ของเรา
// mutation handler functions จะต้องเป็น synchronous เท่านั้น
// ไม่สามารถเรียกตรงๆผ่าน this.loadedPosts ได้ ต้องทำผ่านฟังก์ชั่นสำหรับเปลี่ยน data
// mutations 1 ตัว ต่อ ตัวแปรใน state 1ตัว
export const mutations = {
  // setPosts เป็นฟังก์ชั่นสำหรับเปลี่ยน data
  // param ตัวแรกคือชื่อ state ตัวสองคือชื่อ param ที่สร้างเองเพื่อใช้รับ data ที่ส่งเข้ามา
  setPosts(state, data) {
    state.loadedPosts = data;
    console.log(' state.loadedPosts: ',  state.loadedPosts);
  },
  IS_SIDEBAR_ACTIVE(state, value) {
    state.isSidebarActive = value;
  }
};

// actions มีไว้สั่งให้เกิดการ mutations กับบตัวแปรหลายๆตัวใน state
// คล้ายกับ mutations แต่แทนที่จะแก้ไข data ที่ state ตรงๆ ก็จะใช้วิธี commit mutation แทน
// โค้ดใน action สามารถเป็น aynschronous ได้
export const actions = {
  setPosts(state, data) {
    // setPosts คือ state ที่ประกาศไว้ใน mutations
    // posts ข้อมูลที่รับเข้ามา เมื่อเราเรียก Action นี้ขึ้นมาทํางาน
    // commit เป็น helpper fucthion เพื่อใช้ส่ง data ไปให้ mutations
    state.commit('setPosts', data);
    // ความหมายเหมือนข้างล่างนี้ แต่ต้องใช้วิธีพิเศษตามข้างบนเท่านั้น
    // this.$store.state.setPosts = data
  }
};

// Getters จะใช้ในกรณีที่ต้องการ Process อะไรบางอย่างก่อนที่จะทําข้อมูลใน Store ไปใช้
export const getters = {
  loadedPosts(state) {
    return state.loadedPosts;
  }
};